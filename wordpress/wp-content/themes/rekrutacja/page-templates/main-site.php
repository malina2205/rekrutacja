<?php
/**
 * Template Name: Main Site theme
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area">
		 <div id="content" class="site-content" role="main">
		
			<?php
                                        the_post();
                                        
					// Include the page content template.
					get_template_part( 'content', 'page' );
			?>
			<div class="entry-content" style="display: block;margin-left:auto;margin-right:auto;margin-bottom:20px">
                                <?php            
                                        echo types_render_field("opis", array( ));  
                                        
                                        $img = types_render_field("zdjecie", array("url"=>"true")); 		
			                 echo '<a href=" '.$img.' " rel="lightbox">';
			                 echo types_render_field( "zdjecie", 
			                 array("style"=>"border:1px solid black;padding:20px", "size" => "thumbnail" ) );
			                 echo '</a>';

                                ?>
                        </div>                
									
			
		</div><!-- #content -->

	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
