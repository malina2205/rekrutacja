<?php
/**
 * Template Name: Gallery theme
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area">
		 <div id="content" class="site-content" role="main">
		
			<?php
               the_post();
                                        
					// Include the page content template.
					get_template_part( 'content', 'page' );
	
			?>
			<div class="entry-content" style="display: block;margin-left:auto;margin-right:auto;margin-bottom:20px">
			       <?php          
                                                  
                     $title = types_render_field("tytul", array( ));
                     echo '<h2>'.$title.'</h2>';  

                     $img_array = explode(",", types_render_field("zdjecie", 
			            array("url"=>"true", "separator"=>","))); 		
			            foreach($img_array as $img){
			                 echo '<a href=" '.$img.' " rel="lightbox-gallery">';
			                 echo '<img src="'.$img.'" 
			                  style="border:1px solid black;padding:20px; margin:10px" width="150" height="150" ></img>';
			                 echo '</a>';
			            }
			            
			            
                ?>
                                
         </div>                
									
			
		</div><!-- #content -->

	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
